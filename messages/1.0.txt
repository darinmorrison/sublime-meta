Meta 1.0 Changelog:

 - Changed key chords: the last use of 'super' has been dropped. That makes
   shell exec 'super+k, super+m, s' and python exec 'super+k, super+m, p'

 - Some code cleanup
