import functools
import subprocess
import traceback

import sublime_plugin


class MetaExecPython(sublime_plugin.TextCommand):

    """Execute selections as python code."""

    def replace(self, edit, region, result):
        "Replace the contents of a region with a result converted to a string."
        self.view.replace(edit, region, str(result))

    def run(self, edit):
        "For each selection, execute its contents as python code."
        for region in self.view.sel():
            try:
                exec self.view.substr(region) in \
                    { '_self'   : self
                    , '_edit'   : edit
                    , '_region' : region
                    , 'observe' : functools.partial(self.replace, edit, region)
                    }
            except:
                traceback.print_exc(0)


class MetaExecShell(sublime_plugin.TextCommand):

    """Execute selections as shell command and substitute the results."""

    def _pretty_error(self, error_name, error_msg):
        print '%s (%s) {' % (self.name(), error_name)
        for line in error_msg.rstrip('\n').split('\n'):
            print '\t%s' % line
        print '}'

    def run(self, edit):
        "For each selection, execute its contents as a shell command."
        for region in self.view.sel():
            # switch to shlex when python 2.7 is bundled;
            # utf-8 is buggy with prior versions
            args = self.view.substr(region)
            try:
                process = subprocess.Popen( args
                                          , shell   = True
                                          , stdout  = subprocess.PIPE
                                          , stderr  = subprocess.PIPE
                                          )
                result, error = process.communicate()
                if error != '':
                    self._pretty_error('stderr', error)
                else:
                    self.view.replace(edit, region, str(result))
            except OSError, error:
                self._pretty_error('OSError', str(error))
