# Synopsis #

Sublime-meta is a simple plugin for Sublime Text 2 that allows the user to execute python code or shell commands from a buffer and capture the output.


# Description #

Sublime-meta comes with two commands: `meta_exec_shell` and `meta_exec_python`. Neither take any arguments.

* `meta_exec_python` command will execute the selected text as Python code. Because of the unfortunate way in which Python handles expressions vs. statements, `meta_exec_python` will not normally replace the selection with any output; the code will be evaluated only for its side-effects. **Note: To replace the selected python code with its result evaluated as an expression, the final computation must be wrapped in `observe()`**, e.g.,

            def plus_one(n):
                return n + 1

            observe(plus_one(41))

* `meta_exec_shell` command will execute the selected text as a subprocess and – if the subprocess does not return an error – replace the selection with the output. Errors will be printed to the Sublime Text 2 console.

Both commands work with multiple selections.


# Usage #

## Default Keymaps ##

Both commands are available as keymappings under a similar scheme:

* `meta_exec_python` - key chord "super+k", "super+m", "super+p"

* `meta_exec_shell` - key chord "super+k", "super+m", "super+s"


## Command Palette ##

Both commands are available under the command palette. Generally, `mp` and `ms` serve as good shortcuts once the palette has been opened.
